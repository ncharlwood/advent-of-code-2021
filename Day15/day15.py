
file = open("day15.txt").read()

lines = file.splitlines()
num_cols = len(lines[0])
num_rows = len(lines)

grid = [[int(char) for char in line] for line in lines]
distance_dict = {
    (0,0): 0
}

unvisited = {}
for row_n, row in enumerate(lines):
    for col_n, val in enumerate(row):
        unvisited[(row_n, col_n)] = int(val)

def is_valid_and_unvisited_node(node):
    (row, col) = node
    return row >= 0 and row < num_rows and col >= 0 and col < num_cols and node in unvisited

def get_adjacent_nodes(node):
    (row, col) = node

    adjacent = {
        (row + 1, col),
        (row, col + 1),
        (row - 1, col),
        (row, col - 1),
    }

    return set(filter(is_valid_and_unvisited_node, adjacent))

def get_closest_unvisited_node():
    nearby_nodes = {key:value for key,value in distance_dict.items() if value is not None and key in unvisited}

    smallest = None
    for node in nearby_nodes:
        if smallest is None or distance_dict[node] < distance_dict[smallest]:
            smallest = node

    return smallest

current = (0,0)
destination = (num_rows-1, num_cols-1)

while current:
    base_distance = distance_dict[current]
    for adjacent_node in get_adjacent_nodes(current):
        (row, col) = adjacent_node
        distance_from_node = base_distance + grid[row][col]
        if adjacent_node not in distance_dict or distance_from_node < distance_dict[adjacent_node]:
            distance_dict[adjacent_node] = distance_from_node

    unvisited.pop(current)
    if current == destination:
        break
    
    current = get_closest_unvisited_node()

print(distance_dict[destination])
import sys

STEPS = int(sys.argv[1])

file = open("day11.txt").read()

lines = file.splitlines()
num_cols = len(lines[0])
num_rows = len(lines)

flashes = 0
overflowed = set()
seen = set()

def base_increment(grid):
    for row_num, row in enumerate(grid):
        for col_num, num in enumerate(row):
            grid[row_num][col_num] += 1

            if grid[row_num][col_num] > 9:
                if (row_num, col_num) not in seen:
                    coord = (row_num, col_num)
                    overflowed.add(coord)
                    seen.add(coord)
    return grid

def adjacent_increment(grid, row, col):

    adjacent_coords = [
        (row - 1, col - 1),
        (row - 1, col),
        (row - 1, col + 1),
        (row, col - 1),
        (row, col),
        (row, col + 1),
        (row + 1, col - 1),
        (row + 1, col),
        (row + 1, col + 1),
    ]

    def is_in_grid(tuple):
        return tuple[0] < num_rows and tuple[0] >= 0 and tuple[1] < num_cols and tuple[1] >= 0

    valid_coords = list(filter(is_in_grid, adjacent_coords))

    for coord in valid_coords:
        row = coord[0]
        col = coord[1]

        grid[row][col] += 1

        if grid[row][col] > 9:
            if (row, col) not in seen:
                overflowed.add(coord)
                seen.add(coord)

def reset_grid(grid):
    for row_num, row in enumerate(grid):
        for col_num, num in enumerate(row):
            grid[row_num][col_num] = num if num < 10 else 0


def nice_print(grid):
    for row in grid:
        for num in row:
            print(num, end="")
        print("")
    print("-" * num_cols)

def is_synced(grid):
    number = grid[0][0]
    for row in grid:
        for num in row:
            if num != number:
                return False
    return True
grid = [[int(char) for char in line] for line in lines]

step = 0

while not is_synced(grid):
    step += 1

    grid = base_increment(grid)

    while overflowed:
        (row, col) = overflowed.pop()
        flashes += 1

        if grid[row][col] > 9:
            adjacent_increment(grid, row, col)

    reset_grid(grid)
    seen = set()

    if step == STEPS:
        print("Part 1")
        print("Flashes: {}".format(flashes))


print("Part 2")
print("Steps: {}".format(step))

lines = open("day10.txt").read().splitlines()

corrupt_scoring = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137,
}

incomplete_scoring = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4,
}

pairs = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>',

    ')': '(',
    ']': '[',
    '}': '{',
    '>': '<',
}

corrupt_score = 0
incomplete_scores = []

for line in lines:
    stack = []
    corrupted = False

    for char in line:
        
        if char in ('(', '[', '{', '<') or corrupted:
            stack.append(char)
            continue
        
        item = stack.pop()
        expected = pairs.get(item)
        if expected != char:
            # print("{} Expected {}, but found {} instead".format(line, expected, char))
            corrupted = True
            corrupt_score += corrupt_scoring[char]

    if stack and not corrupted:
        stack.reverse()
        score = 0
        added = ""
        for item in stack:
            item_score = incomplete_scoring[pairs[item]]
            score = 5 * score + item_score
            added += pairs[item]

        incomplete_scores.append(score)
        print("{} complete by adding {}".format(line, added))


incomplete_scores.sort()
incomplete_score = incomplete_scores[len(incomplete_scores)//2]

print("Corrupt score: {}".format(corrupt_score))
print("Incomplete score: {}".format(incomplete_score))





lines = open("day13.txt").read().splitlines()

gap_index = lines.index("")
dots = lines[:gap_index]
folds = [line[11:] for line in lines[gap_index + 1:]]

first_folds = folds[:2]
first_folds.sort()

max_x = 2 * int(first_folds[0].split("=")[-1]) + 1
max_y = 2 * int(first_folds[1].split("=")[-1]) + 1

grid = [[False] * max_x for _ in range(0, max_y)]

for dot in dots:
    x = int(dot.split(",")[0])
    y = int(dot.split(",")[1])
    grid[y][x] = True

def nice_print(grid):
    for row in grid:
        for val in row:
            print("#" if val else ".", end="")
        print("")

def count_dots(grid):
    sum = 0

    for row in grid:
        for val in row:
            sum += 1 if val else 0

    return sum

for fold in folds:
    x_or_y = fold.split("=")[0]
    line = int(fold.split("=")[1])

    is_y = x_or_y == 'y'
    is_x = x_or_y == 'x'

    new_num_rows = line if is_y else len(grid)
    new_num_cols = line if is_x else len(grid[0])

    new_grid = [[grid[y][x] for x in range(0, new_num_cols)] for y in range(0, new_num_rows)]

    for y, row in enumerate(new_grid):
        for x, val in enumerate(row):
            new_y = -y + 2 * line if is_y else y
            new_x = -x + 2 * line if is_x else x
            new_grid[y][x] |= grid[new_y][new_x]

    grid = new_grid

    if fold == folds[0]:
        print("Part 1:")
        print("Number of dots after first fold: {}".format(count_dots(grid)))

print("Part 2:")
nice_print(new_grid)
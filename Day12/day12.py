edges = open("day12.txt").read().splitlines()

dict = {}

for edge in edges:
    node_a = edge.split("-")[0]
    node_b = edge.split("-")[1]

    if not dict.get(node_a): 
        dict[node_a] = set()

    if not dict.get(node_b): 
        dict[node_b] = set()

    dict[node_a].add(node_b)
    dict[node_b].add(node_a)

def has_doubled_checked_small_cave(nodes):
    for key in dict.keys():
        if key.islower() and nodes.count(key) > 1:
            return True
    return False

def get_num_routes(past_nodes, current, double_check = False):
    if current == 'end':
        return 1
    
    if current.islower() and current in past_nodes and (has_doubled_checked_small_cave(past_nodes) if double_check else True):
        return 0

    options = dict[current]
    options.discard("start")

    sum = 0
    for node in options:
        sum += get_num_routes(past_nodes + [current], node, double_check)
    
    return sum

print("Part 1")
print("Number of routes: {}".format(get_num_routes([], 'start')))

print("Part 2")
print("Number of routes: {}".format(get_num_routes([], 'start', double_check=True)))
import re

file = open("input.txt", "r")

lines = [line.strip("\n") for line in file.readlines()]

stacks = {}
separate = lines.index("")

# iterate through line with number of stacks
for idx, char in enumerate(lines[separate-1]):
    if not char.isnumeric():
        continue
        
    stacks[char] = []
    for line in lines[separate - 2::-1]:
        if line[idx].isalpha():
            stacks[char].append(line[idx])

moves = lines[separate + 1:]

for move in moves:

    x = re.search(r"move (\d+) from (\d+) to (\d+)", move)
    number, start, end = x.groups()
    for _ in range(int(number)):

        # copy char off stack
        move = stacks[start][-1]

        # remove from stack
        stacks[start] = stacks[start][:-1]

        # put on new stack
        stacks[end].append(move)

# get the last item off every stack, and convert the array of chars into a string
print("".join([stack[-1] for stack in stacks.values()]))
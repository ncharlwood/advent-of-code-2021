import re

file = open("test-input.txt", "r")

lines = [line.strip("\n") for line in file.readlines()]

stacks = {}
separate = lines.index("")

# iterate through line with number of stacks
for idx, char in enumerate(lines[separate-1]):
    if not char.isnumeric():
        continue
        
    stacks[char] = []
    for line in lines[separate - 2::-1]:
        if line[idx].isalpha():
            stacks[char].append(line[idx])

moves = lines[separate + 1:]
print(stacks)

for move in moves:

    x = re.search(r"move (\d+) from (\d+) to (\d+)", move)
    number_str, start, end = x.groups()
    number = int(number_str)

    # copy char off stack
    move = stacks[start][-number:]

    # remove from stack
    stacks[start] = stacks[start][:-number]

    # put on new stack
    stacks[end].extend(move)


# get the last item off every stack, and convert the array of chars into a string
print("".join([stack[-1] for stack in stacks.values()]))
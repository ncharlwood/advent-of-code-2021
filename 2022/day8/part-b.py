import sys
from typing import Any, List

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

file = open(f"{sys.argv[1]}.txt", "r")

# grid of tree heights
lines = [[int(char) for char in line.strip("\n")] for line in file.readlines()]
visible_trees = [[False for _ in line] for line in lines]

def rotate_clockwise(grid: List[List[Any]]):

    # lucky the grid is a square lol
    output = [[None for _ in line] for line in lines]

    for row_num, row in enumerate(grid):
        for col_num, square in enumerate(row):
            output[col_num][len(lines) - 1 - row_num] = square

    return output

# from down, right, bottom, then left
for _ in range(4):

    # the edge is always visible so start with that as the highest
    highest_trees = [int(char) for char in lines[0]]
    visible_trees[0] = [True for _ in highest_trees]
    
    for row_num, row in enumerate(lines[1:-1], 1):
        is_highest = [cur > high for cur, high in zip(row, highest_trees)]
        visible_trees[row_num] = list(map(lambda a, b: a or b, is_highest, visible_trees[row_num]))

        highest_trees = list(map(max, row, highest_trees))

    visible_trees = rotate_clockwise(visible_trees)
    lines = rotate_clockwise(lines)


def get_score(row_num, col_num, grid: List[List]):

    # this is horrifying
    directions = [
        [grid[row_i][col_num] for row_i in range(row_num-1, -1, -1)], # up
        grid[row_num][col_num+1:], # right
        [grid[row_i][col_num] for row_i in range(row_num+1, len(grid[0]))], # down
        grid[row_num][col_num-1::-1] if col_num > 0 else [], # left
    ]

    start = grid[row_num][col_num]
    scores = [0, 0, 0, 0]

    for idx, direction in enumerate(directions):
        for height in direction:
            scores[idx] += 1
            if start <= height:
                break

    final_score = scores[0]
    for score in scores[1:]:
        final_score *= score

    return final_score


score = 0

for row_num, row in enumerate(visible_trees):
    for col_num, is_visible in enumerate(row):
        score = max(score, get_score(row_num, col_num, lines) if is_visible else 0)

print(score)
import sys
from typing import Any, List

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

file = open(f"{sys.argv[1]}.txt", "r")

# grid of tree heights
lines = [[int(char) for char in line.strip("\n")] for line in file.readlines()]

visible_trees = [[False for _ in line] for line in lines]

def rotate_clockwise(grid: List[List[Any]]):

    # lucky the grid is a square lol
    output = [[None for _ in line] for line in lines]

    for row_num, row in enumerate(grid):
        for col_num, square in enumerate(row):
            output[col_num][len(lines) - 1 - row_num] = square

    return output

# from down, right, bottom, then left
for _ in range(4):

    # the edge is always visible so start with that as the highest
    highest_trees = [int(char) for char in lines[0]]
    visible_trees[0] = [True for _ in highest_trees]
    
    for row_num, row in enumerate(lines[1:-1], 1):
        is_highest = [cur > high for cur, high in zip(row, highest_trees)]
        visible_trees[row_num] = list(map(lambda a, b: a or b, is_highest, visible_trees[row_num]))

        highest_trees = list(map(max, row, highest_trees))

    visible_trees = rotate_clockwise(visible_trees)
    lines = rotate_clockwise(lines)

print(sum([sum(row) for row in visible_trees]))
import sys
from typing import List, Union

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

lines = open(f"{sys.argv[1]}.txt", "r").read().split("\n\n")

def compare(a, b):
    if a == b:
        return None
    return a > b

# returns True/False if we have a result for the top level comparison
# returns None if we need to keep iterating through the line
def elements_in_order(left, right):

    left_is_int = isinstance(left, int)
    right_is_int = isinstance(right, int)

    if left_is_int and right_is_int:
        return compare(left, right)

    if left_is_int:
        left = [left]

    if right_is_int:
        right = [right]

    for left_i, right_i in zip(left, right):
        is_in_order = elements_in_order(left_i, right_i)
        if is_in_order is not None:
            return is_in_order

    return compare(len(left), len(right))
        
sum = 0
for pair_index, line in enumerate(lines, start=1):
    left, right = map(eval, line.splitlines())
    sum += pair_index if elements_in_order(left, right) else 0

print(sum)

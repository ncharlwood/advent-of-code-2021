import sys
from functools import cmp_to_key

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

lines = open(f"{sys.argv[1]}.txt", "r").read().split()
START = "[[2]]"
END = "[[6]]"
lines.extend([START, END])
lines = list(map(eval, lines))

def compare(a, b):
    if a == b:
        return None
    if a < b:
        return -1
    if a > b:
        return 1

# returns True/False if we have a result for the top level comparison
# returns None if we need to keep iterating through the line
def elements_in_order(left, right):

    left_is_int = isinstance(left, int)
    right_is_int = isinstance(right, int)

    if left_is_int and right_is_int:
        return compare(left, right)

    if left_is_int:
        left = [left]

    if right_is_int:
        right = [right]

    for left_i, right_i in zip(left, right):
        is_in_order = elements_in_order(left_i, right_i)
        if is_in_order is not None:
            return is_in_order
        
    #print(f"{left} == {right}, {compare(len(left), len(right))}")
    return compare(len(left), len(right))
        
lines = sorted(lines, key=cmp_to_key(elements_in_order))
base = None
for index, line in enumerate(lines, start=1):
    if line == eval(START):
        base = index
    if line == eval(END):
        print(base * index)
        exit()
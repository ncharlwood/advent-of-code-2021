ROCK = 1
PAPER = 2
SCISSORS = 3

WIN = 6
DRAW = 3
LOSS = 0

move_map = {
    # opponent plays rock
    "A X": SCISSORS,
    "A Y": ROCK,
    "A Z": PAPER,

    # opponent plays paper
    "B X": ROCK,
    "B Y": PAPER,
    "B Z": SCISSORS,

    # opponent plays scissors
    "C X": PAPER,
    "C Y": SCISSORS,
    "C Z": ROCK,
}

point_map = {
    "X": LOSS, # LOSS
    "Y": DRAW, # DRAW
    "Z": WIN, # WIN
}

file = open("input.txt", "r")

rounds = [line.strip() for line in file.readlines()]

points = 0

for moves in rounds:
    [_, result] = moves.split(" ")
    points += point_map[result] + move_map[moves]

print(points)
map = {
    # opponent plays
    "A": "Z", # rock
    "B": "X", # paper
    "C": "Y", # scissors

    # you play
    "X": "C", # rock
    "Y": "A", # paper
    "Z": "B", # scissors
}

point_map = {
    "X": 1, # rock
    "Y": 2, # paper
    "Z": 3, # scissors
}

WIN = 6
DRAW = 3
LOSS = 0

file = open("input.txt", "r")

rounds = [line.strip() for line in file.readlines()]

points = 0

for moves in rounds:
    [them, you] = moves.split(" ")
    points += point_map[you]

    if map[you] == them: # win
        points +=  WIN
    elif map[them] == you: # loss
        points += LOSS
    else:
        points += DRAW

print(points)

        



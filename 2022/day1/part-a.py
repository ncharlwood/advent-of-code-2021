file = open("input.txt", "r")

clean_input = [line.strip() for line in file.readlines()]
clean_input.append("")

highest_value = 0
current_value = 0


for line in clean_input:
    if line:
        current_value += int(line)
    else:
        if current_value > highest_value:
            highest_value = current_value
        
        current_value = 0


print(highest_value)
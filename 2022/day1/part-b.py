file = open("input.txt", "r")

clean_input = [line.strip() for line in file.readlines()]
clean_input.append("")

highest_values = [0, 0, 0]
current_value = 0


for line in clean_input:
    if line:
        current_value += int(line)
    else:
        if current_value > min(highest_values):
            highest_values.append(current_value)
            highest_values = sorted(highest_values)[1:]
        
        current_value = 0


print(sum(highest_values))
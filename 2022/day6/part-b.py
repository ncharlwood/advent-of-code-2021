import sys

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

name = sys.argv[1]

file = open(f"{name}.txt", "r")

line = [line.strip("\n") for line in file.readlines()][0]

# starting from the 4th char, check if current and previous 
# 3 chars are unique, if so the current index is returned
for idx, _ in enumerate(line):
    if idx < 14:
        continue

    if len(set(line[idx-13:idx+1])) == 14:
        print(idx+1)
        break

file = open("input.txt", "r")

rucksacks = [line.strip() for line in file.readlines()]

pairs = []
# get every 3rd element, starting from the 0th, 1st and 2nd, then zip together
# this transforms the full list into a list where groups of three from the input become tuples
groups = zip(rucksacks[0::3], rucksacks[1::3], rucksacks[2::3])

for group in groups:
    a, b, c = group
    common = set(a).intersection(set(b)).intersection(set(c))
    pairs.append(list(common)[0])
    

def score(char: str):
    return ord(char) - 38 if char.isupper() else ord(char) - 96
    

print(sum([score(letter) for letter in pairs]))



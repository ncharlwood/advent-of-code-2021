file = open("input.txt", "r")

rucksacks = [line.strip() for line in file.readlines()]

pairs = []

for rucksack in rucksacks:
    half = len(rucksack)//2

    first = set(rucksack[:half])
    second = set(rucksack[half:])

    pairs.append(list(first.intersection(second))[0])
    

def score(char: str):
    return ord(char) - 38 if char.isupper() else ord(char) - 96
    

print(sum([score(letter) for letter in pairs]))



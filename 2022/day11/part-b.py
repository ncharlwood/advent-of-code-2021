import sys
import re
from dataclasses import dataclass
from typing import List
import time

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

lines = open(f"{sys.argv[1]}.txt", "r").read().split("\n\n")

@dataclass
class Monkey:
    items: List[int]
    op: str
    test: int
    true: int
    false: int
    count = 0

monkeys: List[Monkey] = []

for line in lines:
    matches = re.search(r"Monkey (\d+):\n  Starting items: (.*)\n  Operation: new = old (.*)\n  Test: divisible by (.*)\n    If true: throw to monkey (\d+)\n    If false: throw to monkey (\d+)", line)
    num, item_str, op, test, true, false = matches.groups()

    monkeys.append(Monkey(
        items=list(map(int, item_str.split(","))),
        op=str(op),
        test=int(test),
        true=int(true),
        false=int(false),
    ))

def parse_operation(op: str, old: int):
    sign, num = op.split(" ")

    if num == "old":
        num = old

    if sign == "+":
        return old + int(num)

    if sign == "*":
        return old * int(num)


limit = eval("*".join([str(m.test) for m in monkeys]))

for round in range(10000):
    for monkey in monkeys:
        for item in monkey.items:
            monkey.count += 1
            worry = parse_operation(monkey.op, item) % limit
            if worry % monkey.test == 0:
                monkeys[monkey.true].items.append(worry)
            else:
                monkeys[monkey.false].items.append(worry)

        monkey.items = []

[a, b] = sorted([m.count for m in monkeys])[-2:]
print(a*b)
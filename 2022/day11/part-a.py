import sys
import re
from dataclasses import dataclass
from typing import List

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

lines = open(f"{sys.argv[1]}.txt", "r").read().split("\n\n")

@dataclass
class Monkey:
    items: List[int]
    op: str
    test: int
    true: int
    false: int
    count = 0

monkeys: List[Monkey] = []

for line in lines:
    matches = re.search(r"Monkey (\d+):\n  Starting items: (.*)\n  Operation: (.*)\n  Test: divisible by (.*)\n    If true: throw to monkey (\d+)\n    If false: throw to monkey (\d+)", line)
    num, item_str, op, test, true, false = matches.groups()

    monkeys.append(Monkey(
        items=list(map(int, item_str.split(","))),
        op=str(op),
        test=int(test),
        true=int(true),
        false=int(false),
    ))

for round in range(20):
    for monkey in monkeys:
        for old in monkey.items:
            monkey.count += 1
            new = 0
            exec(monkey.op)
            worry = new//3

            if worry % monkey.test == 0:
                monkeys[monkey.true].items.append(worry)
            else:
                monkeys[monkey.false].items.append(worry)

        monkey.items = []

[a, b] = sorted([m.count for m in monkeys])[-2:]
print(a*b)
import sys
from dataclasses import dataclass, field
from typing import List

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

file = open(f"{sys.argv[1]}.txt", "r")

lines = [line.strip("\n") for line in file.readlines()]

@dataclass
class Node:
    name: str
    size: int = 0
    parent: any = field(repr=False, default=None)
    children: List[any] = field(repr=False, default=None) 

MAX = 100000
pwd = None

# just build a list of nodes since I cant be bothered traversing the tree later
nodes = []

for line in lines:
    groups = line.split(" ")

    # check for command
    if groups[0] == "$":
        if groups[1] == "cd" and groups[2] != "..":
            pwd = Node(name=line[5:], parent=pwd)
            nodes.append(pwd)

        elif groups[1] == "cd" and groups[2] == "..":
            # pwd.parent.size += pwd.size
            pwd = pwd.parent

        if groups[1] == "ls":
            pwd.children = []

    # ls output - dir
    elif groups[0] == "dir":
        new_node = Node(name=groups[1], parent=pwd)
        pwd.children.append(new_node)

    # ls output - file
    else:
        pwd.size += int(groups[0])
        parent = pwd.parent

        while parent:
            parent.size += int(groups[0])
            parent = parent.parent

print(sum([node.size for node in nodes if node.size < MAX]))
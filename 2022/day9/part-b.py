import sys

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

file = open(f"{sys.argv[1]}.txt", "r")

lines = [line.strip("\n") for line in file.readlines()]

rope = [
    (0, 0), # H
    (0, 0), # 1
    (0, 0), # 2
    (0, 0), # 3
    (0, 0), # 4
    (0, 0), # 5
    (0, 0), # 6
    (0, 0), # 7
    (0, 0), # 8
    (0, 0), # 9
]

visited = set()

for line in lines:
    move = line.split(" ")[0]
    distance = int(line.split(" ")[1])

    for _ in range(distance):
        head = rope[0]
        if move == "U":
            head = (head[0], head[1]+1)

        elif move == "D":
            head = (head[0], head[1]-1)

        elif move == "L":
            head = (head[0]-1, head[1])

        elif move == "R":
            head = (head[0]+1, head[1])

        rope[0] = head

        for idx, knot in enumerate(rope[1:], 1):

            tail = knot
            head = rope[idx-1]

            diff_x = head[0] - tail[0]
            diff_y = head[1] - tail[1]

            if abs(diff_x) == 2:
                tail = (tail[0]+(diff_x//2), tail[1])

                if abs(diff_y) == 1:
                    tail = (tail[0], tail[1]+diff_y)

            if abs(diff_y) == 2:
                tail = (tail[0], tail[1]+(diff_y//2))

                if abs(diff_x) == 1:
                    tail = (tail[0]+diff_x, tail[1])

            if idx == len(rope) - 1:
                visited.add(tail)

            rope[idx] = tail
            
print(len(visited))
import sys

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

file = open(f"{sys.argv[1]}.txt", "r")

lines = [line.strip("\n") for line in file.readlines()]

head = (0, 0)
tail = (0, 0)
visited = set()

for line in lines:
    move = line.split(" ")[0]
    distance = int(line.split(" ")[1])

    for _ in range(distance):
        if move == "U":
            head = (head[0], head[1]+1)

        elif move == "D":
            head = (head[0], head[1]-1)

        elif move == "L":
            head = (head[0]-1, head[1])

        elif move == "R":
            head = (head[0]+1, head[1])

        diff_x = head[0] - tail[0]
        diff_y = head[1] - tail[1]

        if abs(diff_x) == 2:
            tail = (tail[0]+(diff_x//2), tail[1])

            if abs(diff_y) == 1:
                tail = (tail[0], tail[1]+diff_y)

        if abs(diff_y) == 2:
            tail = (tail[0], tail[1]+(diff_y//2))

            if abs(diff_x) == 1:
                tail = (tail[0]+diff_x, tail[1])

        visited.add(tail)

print(len(visited))

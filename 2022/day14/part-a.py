import sys
from typing import List, Union

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

lines = open(f"{sys.argv[1]}.txt", "r").read().splitlines()

min_x, max_x, min_y, max_y = float('inf'), 0, float('inf'), 0

for line in lines:
    for coord in line.split(" -> "):
        x, y = map(int, coord.split(","))

        if x < min_x: min_x = x
        if x > max_x: max_x = x
        if y < min_y: min_y = y
        if y > max_y: max_y = y

grid = [["." for _ in range(max_x-min_x+1)] for _ in range(max_y+1)]

for pair in lines:
    coords_str_array = pair.split(" -> ")
    coords = [(int(coord.split(",")[0]), int(coord.split(",")[1])) for coord in coords_str_array]
    for a, b in zip(coords[:-1], coords[1:]): # pairwise
        a_x, a_y = a[0]-min_x, a[1]
        b_x, b_y = b[0]-min_x, b[1]
        grid[a_y][b_x] = "#"

        if a_x == b_x:
            for i in range(min(a_y, b_y), max(a_y, b_y)+1):
                grid[i][a_x] = "#"
        if a_y == b_y:
            for i in range(min(a_x, b_x), max(a_x, b_x)+1):
                grid[a_y][i] = "#"

START = (500 - min_x, 0)
current = START

count = 0

while current[1] < len(grid)-1 and current[0] >= 0 and current[0] < len(grid[0])-1:
    x, y = current

    if grid[y+1][x] == ".":
        current = (x, y+1)
    elif grid[y+1][x-1] == ".":
        current = (x-1, y+1)
    elif grid[y+1][x+1] == ".":
        current = (x+1, y+1)
    else:
        grid[y][x] = "o"
        current = START
        count += 1

print(count)
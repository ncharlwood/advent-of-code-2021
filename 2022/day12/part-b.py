import sys
from dataclasses import dataclass
from typing import List, Set

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

lines = open(f"{sys.argv[1]}.txt", "r").read().splitlines()

grid = [list(line) for line in lines]
visited = [[False for x in line] for line in lines]

HEIGHT = len(grid)
WIDTH = len(grid[0])

@dataclass
class Square:
    col: int
    row: int
    distance: int
    value: int

    def __hash__(self):
        return hash(str(self))

current: Set[Square] = set()
end: Set[Square] = set()

for row_num, row in enumerate(grid):
    for col_num, cell in enumerate(row):
        if cell in ["S", "a"]:
            current.add(Square(col_num, row_num, 0, ord('a')))


def get_adjacent(square: Square) -> List[Square]:
    row = square.row
    col = square.col

    options = [
        (col+1, row),
        (col-1, row),
        (col, row+1),
        (col, row-1),
    ]

    valid = []
    for x, y in options:
        if x >= 0 and y >= 0 and x < WIDTH and y < HEIGHT:
            valid.append(Square(x, y, square.distance+1, ord(grid[y][x])))
    return valid

while current:
    next_batch = set()
    for option in current:
        adjacent = get_adjacent(option)
            
        for square in adjacent:
            if square.value <= option.value + 1 and not visited[square.row][square.col]:
                next_batch.add(square)

            if option.value == ord("z") and square.value == ord("E"):
                end.add(square)

    # mark as visited
    for square in current:
        visited[square.row][square.col] = True

    current = next_batch

if end:
    print(sorted([square.distance for square in end])[0])

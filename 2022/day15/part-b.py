import sys
import re

if len(sys.argv) != 3:
    print("Must provide only one file name")
    exit()

file_name = sys.argv[1]
limit = int(sys.argv[2])

lines = open(f"{file_name}.txt", "r").read().splitlines()

def insert_to_dict(dictionary, key, value):
    if key in dictionary:
        dictionary[key].add(value)
    else:
        dictionary[key] = set([value])


pos_slope_y = {}
neg_slope_y = {}

sensors = []

for line in lines:
    matches = re.search(r"Sensor at x=(.*), y=(.*): closest beacon is at x=(.*), y=(.*)", line)
    x_1, y_1, x_2, y_2 = map(int, matches.groups())

    distance = abs(x_1 - x_2) + abs(y_1 - y_2)
    sensors.append(((x_1, y_1), distance))

    vertical_min = y_1 - distance
    vertical_max = y_1 + distance

    insert_to_dict(pos_slope_y, vertical_max - x_1, "below")
    insert_to_dict(pos_slope_y, vertical_min - x_1, "above")
    insert_to_dict(neg_slope_y, vertical_max + x_1, "below")
    insert_to_dict(neg_slope_y, vertical_min + x_1, "above")

def get_possible_lines(y_intersections: dict):

    result = []
    for key in y_intersections:

        if key+2 not in y_intersections:
            continue

        lower_values = y_intersections[key]
        higher_values = y_intersections[key+2]

        if not("below" in lower_values and "above" in higher_values):
            continue

        result.append(key + 1)

    return result

pos_lines = get_possible_lines(pos_slope_y)
neg_lines = get_possible_lines(neg_slope_y)

def is_in_range(x, y, sensors):
    for (x_b, y_b), distance in sensors:
        if abs(x - x_b) + abs(y - y_b) <= distance:
            return True
    return False

coords = None

for pos in pos_lines:
    for neg in neg_lines:
        x = (neg - pos)//2
        y = x + pos

        if x < 0 or x > limit or y < 0 or y > limit:
            continue

        if not is_in_range(x, y, sensors):
            coords = (x, y)

print(coords[0]*4000000 + coords[1])
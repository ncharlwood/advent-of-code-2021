import sys
import re

if len(sys.argv) != 3:
    print("Must provide only one file name")
    exit()

file_name = sys.argv[1]
sample_row = int(sys.argv[2])

lines = open(f"{file_name}.txt", "r").read().splitlines()

blocked_spots = set()
beacons = set()

for line in lines:
    matches = re.search(r"Sensor at x=(.*), y=(.*): closest beacon is at x=(.*), y=(.*)", line)
    x_1, y_1, x_2, y_2 = map(int, matches.groups())

    distance = abs(x_1 - x_2) + abs(y_1 - y_2)

    vertical_min = y_1 - distance
    vertical_max = y_1 + distance

    if not (vertical_min <= sample_row and sample_row <= vertical_max):
        continue

    dist_to_edge = min(sample_row - vertical_min, vertical_max - sample_row)
    blocked_spots = blocked_spots.union([(x, sample_row) for x in range(x_1 - dist_to_edge, x_1 + dist_to_edge + 1)])
    beacons.add((x_2, y_2))

print(len(blocked_spots - beacons))
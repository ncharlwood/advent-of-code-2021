file = open("input.txt", "r")

pairs = [line.strip() for line in file.readlines()]

count = 0

for pair in pairs:
    [range_a, range_b] = pair.split(",")

    [start_a, end_a] = [int(i) for i in range_a.split("-")]
    [start_b, end_b] = [int(i) for i in range_b.split("-")]

    # check if one range starts or ends within the other
    if (
        (start_a <= start_b and start_b <= end_a) or
        (start_b <= start_a and start_a <= end_b)
    ):
        count += 1

print(count)
import sys

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

file = open(f"{sys.argv[1]}.txt", "r")

lines = file.read().replace("addx", "noop\naddx").splitlines()

register = 1
output = ""

for cycle_count, line in enumerate(lines, start = 1):
    
    instruction = line.split(" ")[0]

    cycle_per_line = cycle_count % 40
    pixel_position = cycle_per_line - 1

    sprite = [register - 1, register, register + 1]
    output += "#" if pixel_position in sprite else "."

    # checking register DURING the cycle
    if cycle_per_line == 0:
        output += "\n"

    if instruction == "addx":
        number = int(line.split(" ")[1])
        register += number

print(output)

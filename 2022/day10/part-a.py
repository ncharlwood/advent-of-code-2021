import sys

if len(sys.argv) != 2:
    print("Must provide only one file name")
    exit()

file = open(f"{sys.argv[1]}.txt", "r")

lines = file.read().replace("addx", "noop\naddx").splitlines()

register = 1
sum = 0

for cycle_count, line in enumerate(lines, start=1):
    instruction = line.split(" ")[0]

    # checking register DURING the cycle
    if cycle_count in [20, 60, 100, 140, 180, 220]:
        sum += cycle_count*register

    if instruction == "addx":
        number = int(line.split(" ")[1])
        register += number
    
print(sum)
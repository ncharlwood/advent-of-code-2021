f = open("day4.txt")

numbers = f.readline()[:-1].split(",")

def toInt(num_str):
    return int(num_str)

numbers = list(map(toInt, numbers))


# clear blank line

boards = []
boards_seen = []
board_won = []

line = f.readline()

# pattern is 5 lines for a board, with a blank line between each board
while(line): 
    board = []
    board_seen = []
    for index in range(0, 5):
        line = list(map(toInt, f.readline().split()))

        board.append(line)
        board_seen.append([False, False, False, False, False])

    # skip blank line
    line = f.readline()

    boards.append(board)
    boards_seen.append(board_seen)
    board_won.append(False)

def isBoardWin(seen_board, row, col):
    if all(seen_board[row]):
        return True

    if all(row[col] for row in seen_board):
        return True
    
    return False

def calculateScore(board, seen, last_num):
    score = 0
    for row_i in range(0, 5):
        for col_i in range(0, 5):
            if not seen[row_i][col_i]:
                score = score + board[row_i][col_i]

    print(score*last_num)
    exit()


for number in numbers:
    for num_board, board in enumerate(boards):
        board_seen = boards_seen[num_board]

        # check board for number
        for row_i in range(0, 5):
            for col_i in range(0, 5):
                if number == board[row_i][col_i]:
                    board_seen[row_i][col_i] = True
                    if isBoardWin(board_seen, row_i, col_i):
                        board_won[num_board] = True
                        if all(board_won):
                            calculateScore(board, board_seen, number)





import sys

STEPS = int(sys.argv[1])

lines = open("day14.txt").read().splitlines()

polymer = lines[0]
rules = lines[2:]

#create rule dictionary
rule_dict = {}
for rule in rules:
    key = rule.split(" -> ")[0]
    value = rule.split(" -> ")[1]
    rule_dict[key] = value

def add_to_dict(dict, key, value):
    dict[key] = dict[key] + value if dict.get(key) else value

char_counts = {
    polymer[0]: 1
}

# count number of pairs in the starting string
# start at one and take pair as item and item before
pair_counts = {}
for index in range(1, len(polymer)):
    pair = polymer[index-1:index+1]
    add_to_dict(pair_counts, pair, 1)
    add_to_dict(char_counts, polymer[index], 1)

# count pairs while doing insertions for each step
for step in range(0, STEPS):
    new_counts = {}

    for pair, count in pair_counts.items():
        insert = rule_dict[pair]
        new_pair1 = pair[0] + insert
        new_pair2 = insert + pair[1]

        add_to_dict(new_counts, new_pair1, count)
        add_to_dict(new_counts, new_pair2, count)
        add_to_dict(char_counts, insert, count)
    pair_counts = new_counts

totals = list(char_counts.values())
totals.sort()
print(totals[-1] - totals[0])
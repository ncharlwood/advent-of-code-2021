file = open("day9.txt").read()

rows = file.splitlines()
grid = [[int(char) for char in row] for row in rows]

num_rows = len(rows)
num_columns = len(rows[0])
seen_grid = [[False] * num_columns for i in range(0, num_rows)]

mins = []

def is_minimum(x, y):
    value = grid[y][x]
    if (
        (y > 0 and value >= grid[y-1][x]) or
        (y < num_rows - 1 and value >= grid[y+1][x]) or
        (x > 0 and value >= grid[y][x-1]) or
        (x < num_columns - 1 and value >= grid[y][x+1])
    ):
        return False
    else:
        return True

for row_num, row in enumerate(grid):
    for col_num, value in enumerate(row):
        if is_minimum(col_num, row_num):
            mins.append((col_num, row_num))


def calc_basin_size(point, index):
    x,y = point

    if grid[y][x] == 9:
        return

    if seen_grid[y][x]:
        return

    basin_sizes[index] += 1
    
    seen_grid[y][x] = True

    if y > 0:
        calc_basin_size((x, y-1), index)
    if y < num_rows - 1:
        calc_basin_size((x, y+1), index)
    if x > 0:
        calc_basin_size((x-1, y), index)
    if x < num_columns - 1:
        calc_basin_size((x+1, y), index)
    return

basin_sizes = [0] * len(mins)
for index, point in enumerate(mins):
    size = 0
    calc_basin_size(point, index)

basin_sizes.sort(reverse = True)

print(basin_sizes[0] * basin_sizes[1] * basin_sizes[2])
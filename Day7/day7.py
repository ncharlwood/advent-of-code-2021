f = open("day7.txt")
line = f.readline()
f.close()

positions = list(map(int, line.split(",")))

positions.sort()

smallest = None
for num in range(positions[0], positions[-1]):
    sum = 0
    for position in positions:
        moves = abs(position - num)
        sum += moves*(moves+1)/2
    smallest = sum if not smallest or sum < smallest else smallest

print(smallest)

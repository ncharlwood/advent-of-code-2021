f = open("day8.txt")

total = 0

line = f.readline()

rules = {
    'abcefg': 0,
    'cf': 1,
    'acdeg': 2,
    'acdfg': 3,
    'bcdf': 4,
    'abdfg': 5,
    'abdefg': 6,
    'acf': 7,
    'abcdefg': 8,
    'abcdfg': 9,
}

total = 0

while (line):
    inputs = line.split("|")[0].split()
    inputs.sort(key=len)

    keys = {
        'a': (set(inputs[1]) - set(inputs[0])).pop(),
    }

    counts = {
        'a': 0,
        'b': 0,
        'c': 0,
        'd': 0,
        'e': 0,
        'f': 0,
        'g': 0,
    }

    chars = "".join(inputs)
    for char in chars:
        counts[char] += 1

    for char, occurances in counts.items():
        if occurances == 4:
            keys.__setitem__('e', char)
        elif occurances == 6:
            keys.__setitem__('b', char)
        elif occurances == 9:
            keys.__setitem__('f', char)
        elif occurances == 8 and char != keys['a']:
            keys.__setitem__('c', char)


    keys['d'] = (set(inputs[2]) - set(keys.values())).pop()
    keys['g'] = (set(inputs[-1]) - set(keys.values())).pop()

    outputs = line.split("|")[1].split()
    clone = {}

    for key, item in rules.items():
        decoded_key = "".join(sorted("".join([keys[char] for char in key])))
        clone.__setitem__(decoded_key, item)

    print(clone)
    print(rules)
    digits = ""
    for output in outputs:
        digits += str(clone["".join(sorted(output))])

    print(digits)

    total += int(digits)        

    line = f.readline()

print(total)

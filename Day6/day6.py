import sys

f = open("day6.txt")
line = f.readline()
f.close()

DAYS = int(sys.argv[1])

fishes = list(map(int, line.split(",")))

fish_counts = [0]*9

for fish in fishes:
    fish_counts[fish] += 1

for day in range(0, DAYS):
    num_new_fish = fish_counts.pop(0)
    fish_counts.append(num_new_fish)
    fish_counts[6] += num_new_fish

print(sum(fish_counts))
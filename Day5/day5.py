f = open("day5.txt")

width = 1000
board = [[0] * width for row in range(0, width)]

line = f.readline()[0:-1]

def hello(board):
    return
    for row in board:
        for val in row:
            print('.' if val == 0 else val, end="")
            
        print()

while(line):
    print(line)
    coords = line.split(" -> ")
    a = list(map(int, coords[0].split(",")))
    b = list(map(int, coords[1].split(",")))

    start_x = a[0] if a[0] < b[0] else b[0]
    end_x = b[0] if a[0] < b[0] else a[0]
    start_y = a[1] if a[1] < b[1] else b[1]
    end_y = b[1] if a[1] < b[1] else a[1]


    diff = (
        a[0] - b[0],
        a[1] - b[1]
    )

    if diff:
        print("Diff: {}".format(diff))
        print("x: {} -> {}".format(start_x, end_x))
        print("y: {} -> {}".format(start_y, end_y))
        print("--------")
        for i in range(0, max(abs(diff[0]), abs(diff[1])) + 1):
            x = min(start_x, end_x) + i if diff[0] < 0 else max(start_x, end_x) - i if diff[0] > 0 else start_x
            y = min(start_y, end_y) + i if diff[1] < 0 else max(start_y, end_y) - i if diff[1] > 0 else start_y
            print("({}, {})".format(x, y))
            board[y][x] += 1
        hello(board)
    else:
        print("skipping")



    line = f.readline()[0:-1]

def countIntersections(board):
    count = 0
    for row in board:
        for val in row:
            if val >= 2:
                count += 1
    return count

print(countIntersections(board))
